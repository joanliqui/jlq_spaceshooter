﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Animator animationRobot;
    public Animator animationButton;
    public void PressPlay(){
        animationButton.SetBool("disapears", true);
        StartCoroutine(Wait());
    }

    IEnumerator Wait(){
        yield return new WaitForSeconds(1.0f);
        animationRobot.SetBool("PlayClick", true);
        yield return new WaitForSeconds (1.05f);
        SceneManager.LoadScene("SampleScene");
    }

    public void PressExit(){
        Application.Quit();
    }
}
