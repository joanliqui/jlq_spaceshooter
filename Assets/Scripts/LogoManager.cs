﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LogoManager : MonoBehaviour
{
    void Start(){
        StartCoroutine(TitleTime());
    }

    IEnumerator TitleTime(){
        yield return new WaitForSecondsRealtime(2.5f);
        SceneManager.LoadScene("TitleScreen");
    }
}
