﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float movementSpeed;
    private Rigidbody rb;

    private float moveHorizontal;
    private float moveVertical;
    private Vector3 movement;

    [SerializeField] Vector2 limits;
    void Awake(){
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate () {
        //Movimiento Por Fisicas
        moveHorizontal = Input.GetAxis("Horizontal");
        moveVertical = Input.GetAxis("Vertical");
        movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement * movementSpeed;
    }

    void Update(){
        //Limites
        if(transform.position.x > limits.x){
            transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
        }
        else if(transform.position.x < -limits.x){
             transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
        }
         if (transform.position.z > limits.y){
            transform.position = new Vector3 (transform.position.x, transform.position.y, limits.y);
        }
        else if(transform.position.z < -limits.y){
            transform.position = new Vector3 (transform.position.x, transform.position.y, -limits.y);
        }
        
    }
}
